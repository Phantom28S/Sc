from enum import Enum
from typing import Callable, Union


class ButtonType(Enum):
    Hat = (u'\U0001F3A9', 'Помощник библиотекаря')
    Back = (u'\U00002B05', 'Назад')
    Poker = (u'\U0001F0CF', 'Меню в столовой')
    Soccer = ('📜', 'Расписание')
    Pizza = (u'\U0001F355', 'Пицца')
    Salad = (u'\U0001F374','Салаты')
    Snack = (u'\U0001F364','Закуски')
    Drink = (u'\U0001F379','Напитки')
    Dessert = (u'\U0001F368','Десерты')
    Bag = (u'\U0001F697','Корзина')
    Delete_Bag = (u'\U0001F697', 'Очистить корзину')
    Pizza1 = (u'\U0001F355','Классик')
    Pizza2 = (u'\U0001F355','Куриная')
    Pizza3 = (u'\U0001F355', 'Мясная')
    Pizza4 = (u'\U0001F355', 'Итальянская')
    Pizza5 = (u'\U0001F355', 'Неаполитанская')
    Pizza_info_yes = (u'\U0001F355', 'Да, выбрать варианты')
    #Pizza_info_no = (u'\U0001F355', 'Нет, выбрать другую')
    Pizza_info_no = (u'\U00002B05', 'Назад')
    Pizza_menu_info_no = (u'\U0001F355', 'Выбрать что-то другое')
    Pizza_bag = (u'\U0001F355', 'Перейти в корзину')
    Pizza_back_menu = (u'\U0001F355', 'Вернуться в меню')
    Pizza_order = (u'\U0001F355', 'Заказать')
    Order_restaurant = (u'\U0001F355', 'Доставить')
    Order_i = (u'\U0001F355', 'Заберу сам')
    Location = (u'\U0001F355', 'Отправить локацию')
    @staticmethod
    def from_string(value: str):
        for name in ButtonType._member_names_:
            member = ButtonType._member_map_[name]
            if str(member) == value:
                return member
        raise ValueError("Not value of enum 'Smile'")

    def __str__(self):
        return "{} {}".format(self.value[0], self.value[1])



class TournamentInfo:
    def __init__(self, has_team_name: bool, team_size: int, check_additional_info: Union[Callable, None]):
        self.has_team_name = has_team_name
        self.team_size = team_size
        self.check_additional_info = check_additional_info


def check_hat_info(info: str):
    return len(info.split()) == 20

soccer_info = TournamentInfo(has_team_name=True, team_size=6, check_additional_info=None)
hat_info = TournamentInfo(has_team_name=False, team_size=2, check_additional_info=check_hat_info)
poker_info = TournamentInfo(has_team_name=False, team_size=1, check_additional_info=None)

