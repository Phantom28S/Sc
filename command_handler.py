
from telebot import TeleBot, types
from telebot.types import Message

import keyboard_helper


class CommandHandler:

    def __init__(self, bot: TeleBot):
        self.bot = bot
        self.file_info = ''

    def start(self, message: Message):
        self.bot.send_photo(message.chat.id, self.file_info)

    def kek(self, message: Message):
        self.file_info = message

    def keyboard_in_tournaments(self, message: Message):
        keyboard = keyboard_helper.create_tournaments_keyboard()
        tournament_list = 'Список функций'
        self._send_keyboard(message.chat.id, tournament_list, keyboard)

    def _send_keyboard(self, chat_id: str, text:str, keyboard: types.ReplyKeyboardMarkup):
        self.bot.send_message(chat_id, text, reply_markup=keyboard, parse_mode="Markdown")